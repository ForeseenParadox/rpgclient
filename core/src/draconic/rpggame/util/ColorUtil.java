package draconic.rpggame.util;

import com.badlogic.gdx.graphics.Color;

public class ColorUtil
{

	public static Color createHSVColor(float h, float s, float v, float a)
	{
		final float C = v * s;
		final float X = C * (1 - Math.abs((h / 60) % 2 - 1));
		float m = v - C;

		float rPrime = 0, gPrime = 0, bPrime = 0;
		if (h < 60)
		{
			rPrime = C;
			gPrime = X;
			bPrime = 0;
		} else if (h < 120)
		{
			rPrime = X;
			gPrime = C;
			bPrime = 0;
		} else if (h < 180)
		{
			rPrime = 0;
			gPrime = C;
			bPrime = X;
		} else if (h < 240)
		{
			rPrime = 0;
			gPrime = X;
			bPrime = C;
		} else if (h < 300)
		{
			rPrime = X;
			gPrime = 0;
			bPrime = C;
		} else if (h < 360)
		{
			rPrime = C;
			gPrime = 0;
			bPrime = X;
		}

		float r = rPrime + m, g = gPrime + m, b = bPrime + m;
		return new Color(r, g, b, a);
	}

}

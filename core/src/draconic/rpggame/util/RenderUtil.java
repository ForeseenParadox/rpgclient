package draconic.rpggame.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.entity.MobileEntity;
import draconic.rpggame.world.entity.SizedEntity;

public class RenderUtil
{

	private static GlyphLayout layout;

	static
	{
		layout = new GlyphLayout();
	}

	public static float getStringWidth(BitmapFont font, String x)
	{
		layout.setText(font, x);
		return layout.width;
	}

	public static float getStringHeight(BitmapFont font, String x)
	{
		layout.setText(font, x);
		return layout.height;
	}

	public static void renderHealthBar(ShapeRenderer shape, MobileEntity mobileEntity, float width, float height)
	{
		shape.set(ShapeType.Line);

		int w = (int) mobileEntity.getScaledWidth(), h = (int) mobileEntity.getScaledHeight();
		Vector2f pos = mobileEntity.getPosition();

		shape.setColor(Color.RED);
		shape.rect(pos.x - (width - w) / 2, pos.y + h + 10, width, height);

		shape.setColor(Color.GREEN);
		shape.rect(pos.x - (width - w) / 2, pos.y + h + 10, (int) (width * mobileEntity.getHealthRatio()), height);
	}

	public static void renderBox(ShapeRenderer shape, SizedEntity se)
	{
		shape.set(ShapeType.Line);
		shape.setColor(Color.BLUE);
		shape.rect(se.getX(), se.getY(), se.getScaledWidth(), se.getScaledHeight());
	}

	public static void renderCircle(ShapeRenderer shape, SizedEntity se, float radius)
	{
		shape.set(ShapeType.Line);
		shape.setColor(Color.GREEN);
		shape.circle(se.getMidX(), se.getMidY(), radius);
	}

	public static void renderLine(ShapeRenderer shape, SizedEntity e1, SizedEntity e2)
	{
		Vector2f m1 = e1.midpoint(), m2 = e2.midpoint();
		shape.setColor(Color.GREEN);
		shape.line(m1.x, m1.y, m2.x, m2.y);
	}

}

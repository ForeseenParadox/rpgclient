package draconic.rpggame.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;

import draconic.rpggame.TextureAnimation;
import draconic.rpggame.TextureUtils;
import draconic.rpggame.util.xml.XMLDocument;
import draconic.rpggame.util.xml.XMLElement;

public class ResourceHandler implements Disposable
{

	private Map<String, Texture> textures;
	private Map<String, TextureRegion> textureRegions;
	private Map<String, TextureAnimation> animations;

	public ResourceHandler()
	{
		textures = new HashMap<String, Texture>();
		textureRegions = new HashMap<String, TextureRegion>();
		animations = new HashMap<String, TextureAnimation>();
	}

	public Texture getTexture(String name)
	{
		return textures.get(name);
	}

	public TextureRegion getTextureRegion(String name)
	{
		return textureRegions.get(name);
	}

	public TextureAnimation getAnimation(String name)
	{
		return animations.get(name);
	}

	public void insertTexture(String name, Texture texture)
	{
		textures.put(name, texture);
	}

	public void insertTextureRegion(String name, TextureRegion textureRegion)
	{
		textureRegions.put(name, textureRegion);
	}

	public void insertAnimation(String name, TextureAnimation animation)
	{
		animations.put(name, animation);
	}

	public void removeTexture(String name)
	{
		textures.remove(name);
	}

	public void removeTextureRegion(String name)
	{
		textureRegions.remove(name);
	}

	public void removeAnimation(String name)
	{
		animations.remove(name);
	}

	public void loadResourceFile(FileHandle handle)
	{
		System.out.println("Loading resource file at " + handle.path());

		// load the xml
		XMLDocument resourceXml = new XMLDocument(handle.read());

		for (XMLElement element : resourceXml.getRootElement().getChildren())
		{
			if (element.getName().equals("SpriteSheet"))
				loadTextureResource(element);
			else if (element.getName().equals("Animation"))
				loadAnimationResource(element);
		}
	}

	private void loadTextureResource(XMLElement root)
	{
		String id = root.getAttribValue("id");
		String src = root.getAttribValue("src");

		System.out.println("Loading sprite sheet resource " + id);

		// load texture resource
		Texture result = new Texture(Gdx.files.internal(src));

		int spriteWidth = Integer.parseInt(root.getAttribValue("spriteWidth")), spriteHeight = Integer.parseInt(root.getAttribValue("spriteHeight"));

		String[] transparencyTokens = root.getAttribValue("transparency").split(",");

		for (String x : transparencyTokens)
		{
			result = TextureUtils.replaceColorWithTransparency(result, new Color(Integer.parseUnsignedInt(x + "FF", 16)));
		}

		insertTexture(id, result);

		loadTextureRegionElements(root, result, spriteWidth, spriteHeight);
		// loadTextureRegionArrayElements(root, result, spriteWidth, spriteHeight);

	}

	private void loadTextureRegionElements(XMLElement root, Texture parent, int spriteWidth, int spriteHeight)
	{
		// load texture elements
		List<XMLElement> textureElements = root.getChildrenByName("Texture");
		for (XMLElement textureElement : textureElements)
		{
			String name = textureElement.getAttribValue("id");
			TextureRegion regionResult = new TextureRegion(parent);

			System.out.println("Loading " + name + " texture region");

			String subX = textureElement.getAttribValue("subX"), subY = textureElement.getAttribValue("subY");
			String subWidth = textureElement.getAttribValue("subWidth"), subHeight = textureElement.getAttribValue("subHeight");

			regionResult.setRegionX(parseCoordinate(textureElement.getAttribValue("x"), spriteWidth) + (subX == null ? 0 : Integer.parseInt(subX)));
			regionResult.setRegionY(parseCoordinate(textureElement.getAttribValue("y"), spriteHeight) + (subY == null ? 0 : Integer.parseInt(subY)));

			String width = textureElement.getAttribValue("width"), height = textureElement.getAttribValue("height");

			if (width == null)
				regionResult.setRegionWidth((subWidth == null ? spriteWidth : Integer.parseInt(subWidth)));
			else
				regionResult.setRegionWidth((subWidth == null ? parseCoordinate(width, spriteWidth) : Integer.parseInt(subWidth)));

			if (height == null)
				regionResult.setRegionHeight((subHeight == null ? spriteHeight : Integer.parseInt(subHeight)));
			else
				regionResult.setRegionHeight((subHeight == null ? parseCoordinate(height, spriteHeight) : Integer.parseInt(subHeight)));

			insertTextureRegion(name, regionResult);
		}
	}

	private int parseCoordinate(String coord, int noUnitMultiplier)
	{
		if (coord.endsWith("px"))
			return Integer.parseInt(coord.substring(0, coord.length() - 2));
		else
			return Integer.parseInt(coord) * noUnitMultiplier;
	}

	private void loadAnimationResource(XMLElement root)
	{
		String id = root.getAttribValue("id"), src = root.getAttribValue("src");
		System.out.println("Loading " + id + " animation");
		int x = 0, y = 0, spritesWide = 0, spritesTall = 0, cellWidth = 0, cellHeight = 0;
		long delay = Long.parseLong(root.getAttribValue("delay"));
		Texture parent = getTexture(src);

		TextureAnimation result = new TextureAnimation(delay);
		List<TextureRegion> unindexedFrames = new ArrayList<TextureRegion>();

		cellWidth = Integer.parseInt(root.getAttribValue("cellWidth"));
		cellHeight = Integer.parseInt(root.getAttribValue("cellHeight"));
		x = parseCoordinate(root.getAttribValue("x"), cellWidth);
		y = parseCoordinate(root.getAttribValue("y"), cellHeight);
		spritesWide = Integer.parseInt(root.getAttribValue("spritesWide"));
		spritesTall = Integer.parseInt(root.getAttribValue("spritesTall"));

		int subX = 0, subY = 0, frameWidth = cellWidth, frameHeight = cellHeight;
		if (root.getAttrib("subX") != null)
			subX = Integer.parseInt(root.getAttribValue("subX"));
		if (root.getAttrib("subY") != null)
			subY = Integer.parseInt(root.getAttribValue("subY"));
		if (root.getAttrib("subWidth") != null)
			frameWidth = Integer.parseInt(root.getAttribValue("subWidth"));
		if (root.getAttrib("subHeight") != null)
			frameHeight = Integer.parseInt(root.getAttribValue("subHeight"));

		for (int xx = 0; xx < spritesWide; xx++)
		{
			for (int yy = 0; yy < spritesTall; yy++)
			{
				TextureRegion regionResult = new TextureRegion(parent);
				regionResult.setRegionX(x + xx * cellWidth + subX);
				regionResult.setRegionY(y + yy * cellHeight + subY);
				regionResult.setRegionWidth(frameWidth);
				regionResult.setRegionHeight(frameHeight);

				unindexedFrames.add(regionResult);
			}
		}

		String indexData = root.getValue();
		if (indexData == null || indexData.trim().length() <= 0)
		{
			for (TextureRegion frame : unindexedFrames)
				result.addFrame(frame);
		} else
		{
			String[] stringIndices = indexData.split(",");
			for (String t : stringIndices)
				result.addFrame(unindexedFrames.get(Integer.parseInt(t.trim())));
		}

		insertAnimation(id, result);
	}

	@Override
	public void dispose()
	{
		disposeAllResources();
	}

	public void disposeAllResources()
	{
		System.out.println("Unloading all textures...");
		for (String textureName : textures.keySet())
			textures.get(textureName).dispose();
	}

}

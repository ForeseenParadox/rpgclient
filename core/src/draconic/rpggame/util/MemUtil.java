package draconic.rpggame.util;

public class MemUtil
{

	public static final double MB = 1024 * 1024;

	public static double getPercentMemUsed()
	{
		return (double) getUsedMemoryMb() / getTotalMemoryMb();
	}

	public static String getPercentMemUsedString()
	{
		return String.format("%.2f", getPercentMemUsed() * 100);
	}

	public static String getUsedMemoryMbString()
	{
		return String.format("%.1f MB", getUsedMemoryMb());
	}

	public static String getTotalMemoryMbString()
	{
		return String.format("%.1f MB", getTotalMemoryMb());
	}

	public static String getMaxMemoryMbString()
	{
		return String.format("%.1f MB", getMaxMemoryMb());
	}

	public static double getUsedMemoryMb()
	{
		return (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / MB;
	}

	public static double getTotalMemoryMb()
	{
		return (Runtime.getRuntime().totalMemory()) / MB;
	}

	public static double getMaxMemoryMb()
	{
		return (Runtime.getRuntime().maxMemory()) / MB;
	}

}

package draconic.rpggame.net;

public class Peer
{

	private String username;
	private long entityId;

	public Peer(String username, long entityId)
	{
		this.username = username;
		this.entityId = entityId;
	}

	public String getUsername()
	{
		return username;
	}

	public long getEntityId()
	{
		return entityId;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public void setEntityId(long entityId)
	{
		this.entityId = entityId;
	}

}
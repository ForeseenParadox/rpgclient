package draconic.rpggame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;

public class TextureUtils
{

	public static Texture replaceMagentaWithTransparency(Texture t)
	{
		return replaceColorWithTransparency(t, new Color(0xFF00FFFF));
	}

	public static Texture replaceColorWithTransparency(Texture t, Color color)
	{
		TextureData dat = t.getTextureData();

		if (!dat.isPrepared())
			dat.prepare();

		Pixmap pixMap = dat.consumePixmap();
		Pixmap modified = new Pixmap(t.getWidth(), t.getHeight(), Format.RGBA8888);

		for (int x = 0; x < t.getWidth(); x++)
		{
			for (int y = 0; y < t.getHeight(); y++)
			{
				if (pixMap.getPixel(x, y) == Color.rgba8888(color))
					modified.drawPixel(x, y, 0);
				else
					modified.drawPixel(x, y, pixMap.getPixel(x, y));
			}
		}

		return new Texture(modified);
	}
}
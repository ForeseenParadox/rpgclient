package draconic.rpggame;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureAnimation
{

	private List<TextureRegion> animationFrames;
	private int currentFrame;

	private long animationDelay;
	private long lastTextureSwitch;

	private boolean started;

	public TextureAnimation(List<TextureRegion> frames, int currentFrame, long animationDelay)
	{
		this.animationFrames = frames;
		this.currentFrame = currentFrame;
		this.animationDelay = animationDelay;
		this.lastTextureSwitch = System.currentTimeMillis();
		started = true;
	}

	public TextureAnimation(TextureRegion texture)
	{
		this.animationFrames = new ArrayList<TextureRegion>();
		animationFrames.add(texture);

		this.currentFrame = 0;
		this.animationDelay = 0;
		this.lastTextureSwitch = System.currentTimeMillis();
		started = true;
	}

	public TextureAnimation(Texture parent, int startX, int startY, int frameWidth, int frameHeight, int framesWide, int framesTall, long delay)
	{
		this(parent, startX, startY, frameWidth, frameHeight, framesWide, framesTall, null, delay);
	}

	public TextureAnimation(Texture parent, int startX, int startY, int frameWidth, int frameHeight, int framesWide, int framesTall, int[] indices, long delay)
	{

		List<TextureRegion> unindexedFrames = new ArrayList<TextureRegion>();
		for (int y = startY; y < startY + (frameHeight * framesTall); y += frameHeight)
		{
			for (int x = startX; x < startX + (frameWidth * framesWide); x += frameWidth)
			{
				TextureRegion frame = new TextureRegion(parent, x, y, frameWidth, frameHeight);
				unindexedFrames.add(frame);
			}
		}

		if (indices == null)
			animationFrames = unindexedFrames;
		else
		{
			animationFrames = new ArrayList<TextureRegion>();
			for (int i : indices)
				animationFrames.add(unindexedFrames.get(i));
		}

		animationDelay = delay;

		lastTextureSwitch = System.currentTimeMillis();

		started = true;
	}

	public TextureAnimation(long delay)
	{
		animationFrames = new ArrayList<TextureRegion>();
		currentFrame = 0;
		animationDelay = delay;
		lastTextureSwitch = System.currentTimeMillis();
		started = true;
	}

	@Override
	public TextureAnimation clone()
	{
		return new TextureAnimation(animationFrames, currentFrame, animationDelay);
	}

	public void addFrame(TextureRegion frame)
	{
		animationFrames.add(frame);
	}

	public void removeFrame(TextureRegion frame)
	{
		animationFrames.remove(frame);
	}

	public long getAnimationDelay()
	{
		return animationDelay;
	}

	public int getCurrentFrameIndex()
	{
		return currentFrame;
	}

	public TextureRegion getCurrentFrame()
	{
		return animationFrames.get(currentFrame);
	}

	public void setAnimationDelay(long animDelay)
	{
		animationDelay = animDelay;
	}

	public void setCurrentFrame(int currentFrame)
	{
		this.currentFrame = currentFrame;
	}

	public void start()
	{
		started = true;
	}

	public void stop()
	{
		started = false;
	}

	public void update(float delta)
	{
		if (!started)
			currentFrame = 0;

		if (System.currentTimeMillis() - lastTextureSwitch >= animationDelay && started)
		{
			currentFrame = (currentFrame + 1) % animationFrames.size();
			lastTextureSwitch = System.currentTimeMillis();
		}
	}

}

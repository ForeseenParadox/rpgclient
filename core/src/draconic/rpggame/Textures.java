package draconic.rpggame;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Textures
{

	private static Map<Integer, TextureRegion> map;

	static
	{
		map = new HashMap<Integer, TextureRegion>();
	}

	private Textures()
	{

	}

	public static TextureRegion getRegion(int id)
	{
		return map.get(id);
	}

	public static final Texture PLAYER = TextureUtils.replaceMagentaWithTransparency(new Texture(Gdx.files.internal("Textures/Player_Test.png")));
	public static final Texture TILES = TextureUtils.replaceMagentaWithTransparency(new Texture(Gdx.files.internal("Textures/TS_ground_various.png")));
	public static final Texture ITEMS = TextureUtils.replaceMagentaWithTransparency(new Texture(Gdx.files.internal("Textures/Items.png")));

}

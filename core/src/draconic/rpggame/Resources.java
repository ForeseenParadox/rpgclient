package draconic.rpggame;

public class Resources
{

	public static final String PROJECTILE_TEXTURE = "Projectile_1";
	public static final String PARTICLE_TEXTURE = "Particle_1";

	public static final String SLIME_ANIMATION = "Slime";

	public static final String PLAYER_LEFT = "Player_Left";
	public static final String PLAYER_RIGHT = "Player_Right";
	public static final String PLAYER_UP = "Player_Up";
	public static final String PLAYER_DOWN = "Player_Down";

}

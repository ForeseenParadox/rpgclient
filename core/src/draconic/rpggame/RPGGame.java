package draconic.rpggame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import draconic.rpggame.gui.ItemImages;
import draconic.rpggame.state.GameStateManager;
import draconic.rpggame.state.MainMenuState;
import draconic.rpggame.state.MultiplayerLoginState;
import draconic.rpggame.state.SplashScreenDisclaimer;
import draconic.rpggame.util.ResourceHandler;

public class RPGGame implements ApplicationListener
{

	private static boolean debug = false;

	public static final int STATE_MAIN_MENU = 2;
	public static final int STATE_MULTIPLAYER_LOGIN = 3;
	public static final int STATE_MULTIPLAYER_SESSION = 3;

	private static RPGGame instance;
	private static SpriteBatch spriteBatch;
	private static ShapeRenderer shapeRenderer;
	private static BitmapFont gameFont20;
	private static BitmapFont gameFont20Markup;

	private static Skin gameSkin;
	private static Skin gameSkinMarkup;

	private static ResourceHandler resourceHandler;

	private GameStateManager manager;

	private double updateDelta;
	private double updateDelay = 1 / 60f;

	public RPGGame()
	{
		instance = this;
	}

	public GameStateManager getManager()
	{
		return manager;
	}

	@Override
	public void create()
	{
		spriteBatch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setAutoShapeType(true);
		resourceHandler = new ResourceHandler();
		resourceHandler.loadResourceFile(Gdx.files.internal("resources.res"));
		manager = new GameStateManager();

		ItemImages.load();

		gameFont20 = createBitmapFont(Gdx.files.internal("Fonts/Neuropol.ttf"), 15);
		gameFont20Markup = createBitmapFont(Gdx.files.internal("Fonts/Neuropol.ttf"), 15);
		gameFont20Markup.getData().markupEnabled = true;

		gameSkin = new Skin();
		gameSkin.addRegions(new TextureAtlas(Gdx.files.internal("Skins/Default/uiskin.atlas")));
		gameSkin.add("default-font", gameFont20, BitmapFont.class);
		gameSkin.load(Gdx.files.internal("Skins/Default/uiskin.json"));

		gameSkinMarkup = new Skin();
		gameSkinMarkup.addRegions(new TextureAtlas(Gdx.files.internal("Skins/Default/uiskin.atlas")));
		gameSkinMarkup.add("default-font", gameFont20Markup, BitmapFont.class);
		gameSkinMarkup.load(Gdx.files.internal("Skins/Default/uiskin-markup.json"));

		// add game states to manager
		manager.addState(new MainMenuState(STATE_MAIN_MENU));
		manager.addState(new MultiplayerLoginState(STATE_MULTIPLAYER_LOGIN));

		manager.pushState(new SplashScreenDisclaimer());

		debug = true;

	}

	public static BitmapFont createBitmapFont(FileHandle handle, int size)
	{
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(handle);
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		BitmapFont font = generator.generateFont(parameter);
		generator.dispose();
		return font;
	}

	@Override
	public void render()
	{

		updateDelta += Gdx.graphics.getDeltaTime();
		while (updateDelta >= updateDelay)
		{
			manager.updateCurrent(Gdx.graphics.getDeltaTime());
			updateDelta -= updateDelay;
		}

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		manager.renderCurrent();
	}

	@Override
	public void resize(int width, int height)
	{
		manager.resizeCurrent(width, height);
	}

	@Override
	public void pause()
	{
		manager.pauseCurrent();
	}

	@Override
	public void resume()
	{
		manager.unpauseCurrent();
	}

	@Override
	public void dispose()
	{

		spriteBatch.dispose();
		gameSkin.dispose();

		manager.disposeCurrent();
	}

	public static RPGGame getInstance()
	{
		return instance;
	}

	public static BitmapFont getGameFont20()
	{
		return gameFont20;
	}

	public static BitmapFont getGameFont20Markup()
	{
		return gameFont20Markup;
	}

	public static SpriteBatch getSpriteBatch()
	{
		return spriteBatch;
	}

	public static ShapeRenderer getShapeRenderer()
	{
		return shapeRenderer;
	}

	public static Skin getGameSkin()
	{
		return gameSkin;
	}

	public static ResourceHandler getResourceHandler()
	{
		return resourceHandler;
	}

	public static Skin getGameSkinMarkup()
	{
		return gameSkinMarkup;
	}

	public static void setGameSkin(Skin gameSkin)
	{
		RPGGame.gameSkin = gameSkin;
	}

	public static boolean isDebug()
	{
		return debug;
	}

	public static void setDebug(boolean debug)
	{
		RPGGame.debug = debug;
	}

}

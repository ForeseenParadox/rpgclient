package draconic.rpggame.gui;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import draconic.rpggame.Textures;
import draconic.rpggame.item.Items;

public class ItemImages
{

	private static Map<Integer, TextureRegion> itemTextures;

	public static void load()
	{
		itemTextures = new HashMap<Integer, TextureRegion>();

		itemTextures.put(Items.IRON_NUGGET, new TextureRegion(Textures.ITEMS, 0, 0, 32, 32));
		itemTextures.put(Items.STICK, new TextureRegion(Textures.ITEMS, 0, 32, 32, 32));
	}

	public static TextureRegion getItemTexture(int itemId)
	{
		return itemTextures.get(itemId);
	}

	private ItemImages()
	{

	}

}

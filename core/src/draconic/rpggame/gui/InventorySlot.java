package draconic.rpggame.gui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import draconic.rpggame.RPGGame;
import draconic.rpggame.item.Item;

public class InventorySlot extends Table
{

	private Image imageComponent;
	private Label amountComponent;

	public InventorySlot()
	{
		imageComponent = new Image();
		add(imageComponent).width(InventoryGUI.SLOT_SIZE).height(InventoryGUI.SLOT_SIZE);
		row();
		amountComponent = new Label("", RPGGame.getGameSkin());
		add(amountComponent).width(InventoryGUI.SLOT_SIZE).height(InventoryGUI.SLOT_SIZE * 0.2f);

		setBackground(RPGGame.getGameSkin().getDrawable("default-pane"));

		imageComponent.addListener(new InputListener()
		{
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor)
			{
				super.enter(event, x, y, pointer, fromActor);
				setBackground(RPGGame.getGameSkin().getDrawable("default-rect"));
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor)
			{
				super.exit(event, x, y, pointer, toActor);
				setBackground(RPGGame.getGameSkin().getDrawable("default-pane"));
			}
		});

	}

	public Image getImageComponent()
	{
		return imageComponent;
	}

	public void clearItem()
	{
		imageComponent.setDrawable(null);
		amountComponent.setText("");
	}

	public void setItem(Item item, int amount)
	{
		if (item == null)
			clearItem();
		else
		{
			imageComponent.setDrawable(new TextureRegionDrawable(ItemImages.getItemTexture(item.getId())));
			amountComponent.setText(Integer.toString(amount));
		}
	}

}

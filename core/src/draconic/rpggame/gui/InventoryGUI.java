package draconic.rpggame.gui;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import draconic.rpggame.RPGGame;
import draconic.rpggame.item.Inventory;
import draconic.rpggame.item.InventoryListener;
import draconic.rpggame.item.Item;

public class InventoryGUI extends Table
{

	public static final int SLOT_SIZE = 56;

	public static final int INVENTORY_WIDTH = 10;
	public static final int INVENTORY_HEIGHT = 10;

	public static final int EQUIP_WIDTH = 1;
	public static final int EQUIP_HEIGHT = 5;

	public static final int ARMOR_WIDTH = 1;
	public static final int ARMOR_HEIGHT = 4;

	private InventorySlot[] invSlots;
	private InventorySlot[] eqSlots;
	private InventorySlot[] arSlots;

	public InventoryGUI(String title, Skin skin, final Inventory inv)
	{
		super();
		setFillParent(true);

		invSlots = new InventorySlot[INVENTORY_WIDTH * INVENTORY_HEIGHT];
		eqSlots = new InventorySlot[EQUIP_WIDTH * EQUIP_HEIGHT];
		arSlots = new InventorySlot[ARMOR_WIDTH * ARMOR_HEIGHT];

		Table inventoryRoot = new Table();
		inventoryRoot.setBackground(RPGGame.getGameSkin().getDrawable("textfield"));

		Table invGui = new Table();
		Table guiSlots = new Table();
		invGui.add(new Label("Inventory", RPGGame.getGameSkin()));
		invGui.row();

		guiSlots.defaults().width(SLOT_SIZE).height(SLOT_SIZE).pad(4).top().right();
		for (int i = 0; i < INVENTORY_HEIGHT; i++)
		{
			for (int j = 0; j < INVENTORY_WIDTH; j++)
			{
				InventorySlot slot = new InventorySlot();
				invSlots[j + i * INVENTORY_WIDTH] = slot;
				guiSlots.add(slot);
			}
			guiSlots.row();
		}

		invGui.add(guiSlots).pad(50);

		inventoryRoot.add(invGui);

		Table equipGui = new Table();
		Table equipSlots = new Table();
		equipGui.add(new Label("Equip", RPGGame.getGameSkin()));
		equipGui.row();
		equipSlots.defaults().width(SLOT_SIZE).height(SLOT_SIZE).pad(5);
		for (int i = 0; i < EQUIP_HEIGHT; i++)
		{
			for (int j = 0; j < EQUIP_WIDTH; j++)
			{
				InventorySlot slot = new InventorySlot();
				eqSlots[j + i * EQUIP_WIDTH] = slot;
				equipSlots.add(slot);
			}
			equipSlots.row();
		}
		equipGui.add(equipSlots).pad(50);
		inventoryRoot.add(equipGui);

		inventoryRoot.top();

		Table armorGui = new Table();
		Table armorSlots = new Table();
		armorGui.add(new Label("Armor", RPGGame.getGameSkin()));
		armorGui.row();
		armorSlots.defaults().width(SLOT_SIZE).height(SLOT_SIZE).pad(5);
		for (int i = 0; i < ARMOR_HEIGHT; i++)
		{
			for (int j = 0; j < ARMOR_WIDTH; j++)
			{
				InventorySlot slot = new InventorySlot();
				arSlots[j + i * ARMOR_WIDTH] = slot;
				armorSlots.add(slot);
			}
			armorSlots.row();
		}
		armorGui.add(armorSlots).pad(50);
		inventoryRoot.top();
		inventoryRoot.add(armorGui);

		add(inventoryRoot);

		inv.addListener(new InventoryListener()
		{

			@Override
			public void itemsAdded(Item item, int amount, int slot)
			{
				invSlots[slot].setItem(item, inv.getItemStackAt(slot).getCount());
			}

			@Override
			public void itemsRemoved(Item item, int amount, int slot)
			{
				invSlots[slot].setItem(item, inv.getItemStackAt(slot).getCount());
			}

			@Override
			public void slotCleared(int slot)
			{
				invSlots[slot].clearItem();
			}

		});

	}

}

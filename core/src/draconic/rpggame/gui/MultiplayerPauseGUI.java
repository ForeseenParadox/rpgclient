package draconic.rpggame.gui;

import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import draconic.rpggame.RPGGame;

public class MultiplayerPauseGUI extends Table
{

	private TextButton returnButton;
	private TextButton optionsButton;
	private TextButton disconnectButton;

	public MultiplayerPauseGUI()
	{

		Table buttonTable = new Table();
		returnButton = new TextButton("Return", RPGGame.getGameSkin());
		optionsButton = new TextButton("Options", RPGGame.getGameSkin());
		disconnectButton = new TextButton("Disconnect", RPGGame.getGameSkin());
		buttonTable.defaults().width(400).height(40).padBottom(10);
		buttonTable.row();
		buttonTable.add(returnButton);
		buttonTable.row();
		buttonTable.add(optionsButton);
		buttonTable.row();
		buttonTable.add(disconnectButton);

		buttonTable.setBackground(RPGGame.getGameSkin().getDrawable("textfield"));
		buttonTable.pad(20);

		center();
		add(buttonTable);
		setFillParent(true);
	}

	public void addReturnButtonListener(EventListener listener)
	{
		returnButton.addListener(listener);
	}

	public void addOptionsButtonListener(EventListener listener)
	{
		optionsButton.addListener(listener);
	}

	public void addDisconnectButtonListener(EventListener listener)
	{
		disconnectButton.addListener(listener);
	}

}

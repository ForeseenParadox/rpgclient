package draconic.rpggame.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import draconic.rpggame.RPGGame;

public class OptionsState extends GameState
{

	private Stage gui;
	private CheckBox debugCheckBox;

	public OptionsState()
	{
		super(-1);
	}

	private void loadCurrentOptions()
	{
		debugCheckBox.setChecked(RPGGame.isDebug());
	}

	@Override
	public void init()
	{
		gui = new Stage(new ScreenViewport(), RPGGame.getSpriteBatch());

		Table root = new Table();
		root.setFillParent(true);
		root.center();

		Table options = new Table();
		options.pad(10);
		options.setBackground(RPGGame.getGameSkin().getDrawable("textfield"));
		debugCheckBox = new CheckBox("Debug mode", RPGGame.getGameSkin());

		loadCurrentOptions();

		debugCheckBox.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				if (debugCheckBox.isChecked())
					RPGGame.setDebug(true);
				else
					RPGGame.setDebug(false);
			}
		});
		options.add(debugCheckBox);

		root.add(options);

		gui.addActor(root);

		Gdx.input.setInputProcessor(gui);
	}

	@Override
	public void update(float delta)
	{
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE))
			RPGGame.getInstance().getManager().popState();
	}

	@Override
	public void resize(int width, int height)
	{

	}

	@Override
	public void pause()
	{

	}

	@Override
	public void unpause()
	{

	}

	@Override
	public void render()
	{
		gui.draw();
	}

	@Override
	public void dispose()
	{

	}

}

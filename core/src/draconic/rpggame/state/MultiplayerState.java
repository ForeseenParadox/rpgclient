package draconic.rpggame.state;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import draconic.rpggame.RPGGame;
import draconic.rpggame.gui.InventoryGUI;
import draconic.rpggame.gui.MultiplayerPauseGUI;
import draconic.rpggame.item.Items;
import draconic.rpggame.net.PacketType;
import draconic.rpggame.util.MemUtil;
import draconic.rpggame.world.ClientWorld;
import draconic.rpggame.world.entity.ClientPlayer;

public class MultiplayerState extends GameState
{

	private static MultiplayerState instance;

	private Thread packetListener;
	private boolean connected;

	private OrthographicCamera worldCam;
	private ClientWorld world;

	private InputMultiplexer multiplexor;

	// gui
	private Stage guiScene;

	private Table chat;
	private Label chatTextArea;
	private TextField chatTextField;
	private ScrollPane scrollPane;

	private MultiplayerPauseGUI pauseGui;
	private InventoryGUI invGui;
	private GUIState guiState = GUIState.HUD;

	private Table dialogBoxRoot;
	private Label dialogBox;

	private MultiplayerSession session;

	private enum GUIState
	{
		HUD, INVENTORY, CHAT, PAUSE, DIALOG;
	}

	public MultiplayerState(MultiplayerSession session)
	{
		super(RPGGame.STATE_MULTIPLAYER_SESSION);
		instance = this;
		this.session = session;

		// initialize and add the client player to the world
		world = session.getWorld();
		world.addEntity(getClientPlayer());
		world.setClientId(session.getClientPlayer().getSerialId());

		connected = true;

		packetListener = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					while (connected)
					{
						int packetId = getSocketInput().readInt();
						if (packetId == PacketType.MESSAGE)
						{
							String message = getSocketInput().readUTF();
							appendChatMessage(message);
						} else if (packetId == PacketType.DISCONNECT)
						{
							serverClosed(getSocketInput().readUTF());
						} else if (packetId == PacketType.INVENTORY_ADD)
						{
							if (getSocketInput().readLong() == getClientPlayer().getSerialId())
							{
								int itemId = getSocketInput().readInt();
								int amount = getSocketInput().readInt();
								getClientPlayer().getInventory().addItem(Items.getItem(itemId), amount);
							}
						} else
						{

							// the world will handle the packet
							world.networkUpdate(packetId, getSocketInput());
						}
					}
				} catch (IOException e)
				{
					if (connected)
					{
						// error
						System.out.println("Disconnected from server: " + e.getMessage());
						serverClosed(e.getMessage());
					}
				}

			}
		});
	}

	public OrthographicCamera getWorldCam()
	{
		return worldCam;
	}

	public boolean isConnected()
	{
		return connected;
	}

	public static MultiplayerState getInstance()
	{
		return instance;
	}

	public MultiplayerSession getSession()
	{
		return session;
	}

	public String getUser()
	{
		return session.getUsername();
	}

	public Socket getConnection()
	{
		return session.getConnection();
	}

	public DataInputStream getSocketInput()
	{
		return session.getDataIn();
	}

	public DataOutputStream getSocketOutput()
	{
		return session.getDataOut();
	}

	public ClientPlayer getClientPlayer()
	{
		return session.getClientPlayer();
	}

	private void serverClosed(String message)
	{
		Socket connection = getConnection();
		System.err.println("Server closed: " + message);
		RPGGame.getInstance().getManager().clearStackTo(RPGGame.STATE_MAIN_MENU);
		connected = false;
		try
		{
			if (!connection.isClosed())
				connection.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void disconnect()
	{
		Socket connection = getConnection();
		System.out.println("Disconnecting from " + connection.getInetAddress().getHostAddress() + ":" + connection.getPort());

		RPGGame.getInstance().getManager().popState();

		connected = false;
		// send final disconnect packet
		try
		{
			if (!connection.isClosed())
			{
				getSocketOutput().writeInt(PacketType.DISCONNECT);
				connection.close();
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public void appendChatMessage(String message)
	{
		chatTextArea.setText(chatTextArea.getText() + message + "\n");
		scrollPane.setScrollPercentY(100);
	}

	public void startDialogMode(String message)
	{
		guiState = GUIState.DIALOG;
		dialogBox.setText(message);
	}

	@Override
	public void init()
	{
		worldCam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		worldCam.zoom = 0.7f;

		InputAdapter worldInputProcessor = new InputAdapter()
		{
			@Override
			public boolean scrolled(int amount)
			{
				if (guiScene.getScrollFocus() != scrollPane)
				{
					worldCam.zoom += (float) amount * 0.04f;

					worldCam.zoom = Math.max(0.3f, worldCam.zoom);
					// worldCam.zoom = Math.min(1f, worldCam.zoom);
				}
				return super.scrolled(amount);
			}
		};

		guiScene = new Stage(new ScreenViewport(), RPGGame.getSpriteBatch());

		// initialize gui components
		invGui = new InventoryGUI("Inventory", RPGGame.getGameSkin(), getClientPlayer().getInventory());
		pauseGui = new MultiplayerPauseGUI();
		invGui.setVisible(false);
		pauseGui.setVisible(false);

		pauseGui.addReturnButtonListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				guiState = GUIState.HUD;
			}
		});
		pauseGui.addOptionsButtonListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				RPGGame.getInstance().getManager().pushState(new OptionsState());
			}
		});
		pauseGui.addDisconnectButtonListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				disconnect();
			}
		});

		chatTextField = new TextField("", RPGGame.getGameSkin());
		chatTextField.addListener(new InputListener()
		{
			@Override
			public boolean keyDown(InputEvent event, int keycode)
			{
				if (keycode == Keys.ENTER)
				{
					if (chatTextField.getText().length() > 0)
					{
						// send chat packet message
						try
						{
							getSocketOutput().writeInt(PacketType.MESSAGE);
							getSocketOutput().writeUTF(chatTextField.getText() + "[WHITE]");
						} catch (IOException e)
						{
							e.printStackTrace();
						}
						chatTextField.setText("");
						guiState = GUIState.HUD;
					}
				}
				return super.keyDown(event, keycode);
			}
		});
		chatTextField.addListener(new FocusListener()
		{
			@Override
			public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused)
			{
				super.keyboardFocusChanged(event, actor, focused);

				if (focused)
				{
					guiState = GUIState.CHAT;
					guiScene.setScrollFocus(scrollPane);
				} else
				{
					guiState = GUIState.HUD;
					guiScene.setScrollFocus(null);
				}
			}
		});

		// hud(chat for now)
		Table hudRoot = new Table();
		hudRoot = new Table(RPGGame.getGameSkin());
		hudRoot.setFillParent(true);

		chat = new Table(RPGGame.getGameSkin());
		chat.defaults().left();

		chatTextArea = new Label("", RPGGame.getGameSkinMarkup());
		chatTextArea.setWrap(true);
		chatTextArea.setAlignment(Align.topLeft);

		scrollPane = new ScrollPane(chatTextArea);
		chat.add(scrollPane).width(800).height(200);
		chat.row();
		chat.add(chatTextField).width(300).height(30);

		hudRoot.left().bottom();
		hudRoot.add(chat);

		// dialog box
		dialogBoxRoot = new Table();
		dialogBoxRoot.setVisible(false);
		dialogBoxRoot.setFillParent(true);
		Table dialogBoxComponent = new Table();
		dialogBoxComponent.setBackground(RPGGame.getGameSkinMarkup().getDrawable("textfield"));
		dialogBoxComponent.bottom().center();
		dialogBox = new Label("", RPGGame.getGameSkinMarkup());
		dialogBox.setWrap(true);
		dialogBox.setAlignment(Align.topLeft);
		dialogBoxComponent.add(dialogBox).width(400).height(100);
		dialogBoxRoot.add(dialogBoxComponent);

		// add actors to gui scene
		guiScene.addActor(hudRoot);
		guiScene.addActor(dialogBoxRoot);
		guiScene.addActor(invGui);
		guiScene.addActor(pauseGui);

		multiplexor = new InputMultiplexer(worldInputProcessor, guiScene);
		packetListener.start();
	}

	@Override
	public void resize(int width, int height)
	{
		worldCam.setToOrtho(false, width, height);
		guiScene.getViewport().update(width, height, true);
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void unpause()
	{
	}

	@Override
	public void update(float delta)
	{
		Gdx.input.setInputProcessor(multiplexor);

		if (guiState == GUIState.HUD)
		{
			pauseGui.setVisible(false);
			invGui.setVisible(false);
			guiScene.setKeyboardFocus(null);
			guiScene.setScrollFocus(null);
			dialogBoxRoot.setVisible(false);

			// update and send player information
			getClientPlayer().update(delta);
			getClientPlayer().serialize(getSocketOutput(), PacketType.ENTITY_UPDATE);

			if (Gdx.input.isKeyJustPressed(Keys.ESCAPE))
				guiState = GUIState.PAUSE;
			if (Gdx.input.isKeyJustPressed(Keys.E))
				guiState = GUIState.INVENTORY;
			if (Gdx.input.isKeyJustPressed(Keys.T))
				guiState = GUIState.CHAT;
		} else if (guiState == GUIState.INVENTORY)
		{
			invGui.setVisible(true);
			guiScene.setScrollFocus(invGui);
			guiScene.setKeyboardFocus(invGui);

			if (Gdx.input.isKeyJustPressed(Keys.E))
				guiState = GUIState.HUD;
		} else if (guiState == GUIState.CHAT)
		{
			guiScene.setKeyboardFocus(chatTextField);
			if (Gdx.input.isKeyJustPressed(Keys.ESCAPE))
				guiState = GUIState.HUD;
		} else if (guiState == GUIState.PAUSE)
		{
			pauseGui.setVisible(true);
			if (Gdx.input.isKeyJustPressed(Keys.ESCAPE))
				guiState = GUIState.HUD;
		} else if (guiState == GUIState.DIALOG)
		{
			dialogBoxRoot.setVisible(true);
			if (Gdx.input.isKeyJustPressed(Keys.ESCAPE))
				guiState = GUIState.HUD;
		}

		worldCam.position.set(new Vector3(getClientPlayer().getMidX(), getClientPlayer().getMidY(), 0));
		worldCam.update();
		world.update(delta);

		// remove and add new entities
		guiScene.act(delta);
	}

	private void renderDebug(SpriteBatch batch, BitmapFont font)
	{
		final int SPACING = 15;
		font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, Gdx.graphics.getHeight() - 20 - 0 * SPACING);
		font.draw(batch, "Entity ID: " + getClientPlayer().getSerialId(), 10, Gdx.graphics.getHeight() - 20 - 1 * SPACING);
		font.draw(batch, "Entity count: " + world.getEntityCount(), 10, Gdx.graphics.getHeight() - 20 - 2 * SPACING);
		font.draw(batch, "Username: " + getUser(), 10, Gdx.graphics.getHeight() - 20 - 3 * SPACING);
		font.draw(batch, "Used memory: " + MemUtil.getUsedMemoryMbString() + "(%" + MemUtil.getPercentMemUsedString() + ")", 10, Gdx.graphics.getHeight() - 20 - 4 * SPACING);
		font.draw(batch, "Total Memory: " + MemUtil.getTotalMemoryMbString(), 10, Gdx.graphics.getHeight() - 20 - 5 * SPACING);
		font.draw(batch, "Max Memory: " + MemUtil.getMaxMemoryMbString(), 10, Gdx.graphics.getHeight() - 20 - 6 * SPACING);
	}

	@Override
	public void render()
	{
		SpriteBatch batch = RPGGame.getSpriteBatch();
		ShapeRenderer shapeRenderer = RPGGame.getShapeRenderer();

		// darken the world if paused or in inventory
		world.render(batch, shapeRenderer, worldCam);

		// render gui scene
		batch.setProjectionMatrix(guiScene.getViewport().getCamera().combined);
		batch.begin();
		if (RPGGame.isDebug())
			renderDebug(batch, RPGGame.getGameFont20());
		batch.end();

		guiScene.draw();
	}

	@Override
	public void dispose()
	{

	}

}

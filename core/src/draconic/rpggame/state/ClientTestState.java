package draconic.rpggame.state;

import draconic.rpggame.RPGGame;
import server.Server;

public class ClientTestState extends GameState
{

	private Server clientServer;
	private MultiplayerState multiplayer;

	public ClientTestState()
	{
		super(-1);

	}

	@Override
	public void init()
	{
		clientServer = new Server("Server/server.proeprties");
		clientServer.run();
		MultiplayerSession session = new MultiplayerSession("Local", "localhost", clientServer.getLocalPort());
		multiplayer = new MultiplayerState(session);
		RPGGame.getInstance().getManager().pushState(multiplayer);
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void unpause()
	{
	}

	@Override
	public void update(float delta)
	{
		if (!multiplayer.isConnected())
		{
			clientServer.stop();
			dispose();
			RPGGame.getInstance().getManager().clearStackTo(RPGGame.STATE_MAIN_MENU);
		}
	}

	@Override
	public void render()
	{
	}

	@Override
	public void dispose()
	{
	}

}

package draconic.rpggame.state;

public abstract class GameState
{

	private int id;

	public GameState(int id)
	{
		this.id = id;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public abstract void init();

	public abstract void update(float delta);

	public abstract void resize(int width, int height);

	public abstract void pause();

	public abstract void unpause();

	public abstract void render();
	
	public abstract void dispose();

}

package draconic.rpggame.state;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class GameStateManager
{

	private Map<Integer, GameState> states;
	private Stack<GameState> stateStack;

	public GameStateManager()
	{
		states = new HashMap<Integer, GameState>();
		stateStack = new Stack<GameState>();
	}

	public void addState(GameState state)
	{
		states.put(state.getId(), state);
		state.init();
	}

	public void removeState(GameState state)
	{
		states.remove(state);
		state.dispose();
	}

	public void clearStack()
	{
		while (!stateStack.isEmpty())
			stateStack.pop();
	}

	public void clearStackTo(int id)
	{
		clearStack();
		pushState(id);
	}

	public void clearStackTo(GameState state)
	{
		clearStack();
		pushState(state);
	}

	public void pushState(int id)
	{
		GameState state = states.get(id);
		if (state != null)
		{
			if (!stateStack.isEmpty())
				pauseCurrent();
			stateStack.push(state);
		} else
		{
			throw new IllegalStateException("GameState with id " + id + " has not been added to the GameStateManager.");
		}
	}

	public void pushState(GameState s)
	{
		if (!stateStack.isEmpty())
			pauseCurrent();
		stateStack.push(s);
		if (!states.containsKey(s))
			s.init();
	}

	public void popState()
	{
		if (!stateStack.isEmpty())
		{
			stateStack.pop();
			if (!stateStack.isEmpty())
				unpauseCurrent();
		}
	}

	public GameState getCurrent()
	{
		return stateStack.peek();
	}

	public void initCurrent()
	{
		getCurrent().init();
	}

	public void resizeCurrent(int width, int height)
	{
		getCurrent().resize(width, height);
	}

	public void pauseCurrent()
	{
		getCurrent().pause();
	}

	public void unpauseCurrent()
	{
		getCurrent().unpause();
	}

	public void renderCurrent()
	{
		getCurrent().render();
	}

	public void updateCurrent(float delta)
	{
		getCurrent().update(delta);
	}

	public void disposeCurrent()
	{
		getCurrent().dispose();
	}

}

package draconic.rpggame.state;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import draconic.rpggame.net.FileData;
import draconic.rpggame.net.FileTransfer;
import draconic.rpggame.net.PacketType;
import draconic.rpggame.world.ClientWorld;
import draconic.rpggame.world.ClientWorldLoader;
import draconic.rpggame.world.entity.ClientEntityFactory;
import draconic.rpggame.world.entity.ClientPlayer;
import draconic.rpggame.world.entity.Entity;

public class MultiplayerSession
{

	private Socket connection;
	private DataOutputStream dataOut;
	private DataInputStream dataIn;
	private ClientPlayer clientPlayer;
	private ClientWorld world;
	private String username;
	private boolean connected;

	public MultiplayerSession(String username, String ip, int port)
	{
		this.username = username;
		this.connection = new Socket();
		try
		{

			System.out.println("Connecting to " + ip + ":" + port);
			connection.connect(new InetSocketAddress(InetAddress.getByName(ip), port), 5000);

			if (connection.isConnected())
			{
				// send player information
				dataOut = new DataOutputStream(connection.getOutputStream());
				dataIn = new DataInputStream(connection.getInputStream());

				dataOut.writeInt(PacketType.PLAYER_INFO);
				dataOut.writeUTF(username);

				// get world data
				// read world name and resource count
				String worldName = dataIn.readUTF();
				int resourceCount = dataIn.readInt();
				FileHandle parent = Gdx.files.local("Maps/" + worldName);

				// receive and load world file into storage
				FileData worldFileData = FileTransfer.receiveFile(dataIn);
				File worldFile = parent.child(worldFileData.getName()).file();
				parent.child(worldFile.getName()).writeBytes(worldFileData.getData(), false);

				// receive and load resource files into storage
				for (int i = 0; i < resourceCount; i++)
				{
					FileData resourceFileData = FileTransfer.receiveFile(dataIn);
					parent.child(resourceFileData.getName()).writeBytes(resourceFileData.getData(), false);
				}
				world = new ClientWorldLoader().loadWorld(worldName, worldFile.getPath());

				// read packet id
				int packetId = dataIn.readInt();
				if (packetId == PacketType.PLAYER_INFO)
				{
					// read entity id
					long id = dataIn.readLong();
					long type = dataIn.readInt();
					if (type == Entity.PLAYER)
					{
						clientPlayer = (ClientPlayer) ClientEntityFactory.createEntity(id, Entity.PLAYER, world, dataIn);
						clientPlayer.deserialize(dataIn);
					}
				} else
				{
					System.err.println("Received incorrect packet header from server! Expected " + Integer.toHexString(PacketType.PLAYER_INFO) + " but received " + Integer.toHexString(packetId));
				}
			}
			connected = true;
		} catch (NumberFormatException e)
		{
			e.printStackTrace();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public String getUsername()
	{
		return username;
	}

	public Socket getConnection()
	{
		return connection;
	}

	public ClientPlayer getClientPlayer()
	{
		return clientPlayer;
	}

	public DataInputStream getDataIn()
	{
		return dataIn;
	}

	public ClientWorld getWorld()
	{
		return world;
	}

	public DataOutputStream getDataOut()
	{
		return dataOut;
	}

	public boolean isConnected()
	{
		return connected;
	}

	public String getDestinationString()
	{
		return getConnection().getInetAddress().getHostAddress() + ":" + getConnection().getPort();
	}

}

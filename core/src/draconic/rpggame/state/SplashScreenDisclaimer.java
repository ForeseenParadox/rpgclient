package draconic.rpggame.state;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import draconic.rpggame.RPGGame;

public class SplashScreenDisclaimer extends GameState
{

	public static final long DISPLAY_LENGTH = 1000;

	private long timeCreated;

	private Stage stage;
	private Table root;
	private Label messageLabel;

	public SplashScreenDisclaimer()
	{
		super(-1);
		timeCreated = System.currentTimeMillis();
	}

	@Override
	public void init()
	{
		stage = new Stage(new ScreenViewport(), RPGGame.getSpriteBatch());
		root = new Table();
		root.setFillParent(true);
		messageLabel = new Label("This is an early release version and is prone to bugs. Draconic will not claim responsibility for any damage caused by using this application.", RPGGame.getGameSkin());
		messageLabel.setWrap(true);
		messageLabel.setAlignment(Align.center);
		root.add(messageLabel).width(900).center();
		stage.addActor(root);
	}

	@Override
	public void update(float delta)
	{
		stage.act(delta);

		if (System.currentTimeMillis() - timeCreated >= DISPLAY_LENGTH)
			RPGGame.getInstance().getManager().clearStackTo(RPGGame.STATE_MAIN_MENU);
	}

	@Override
	public void resize(int width, int height)
	{
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void unpause()
	{

	}

	@Override
	public void render()
	{
		if (RPGGame.getSpriteBatch().isDrawing())
			RPGGame.getSpriteBatch().end();
		stage.draw();
	}

	@Override
	public void dispose()
	{

	}

}

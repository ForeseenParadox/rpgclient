package draconic.rpggame.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import draconic.rpggame.RPGGame;

public class MultiplayerLoginState extends GameState
{
	private Stage guiScene;

	private TextField usernameField;
	private TextField ipField;
	private TextField portField;
	private TextButton connectButton;
	private TextButton backButton;

	public MultiplayerLoginState(int id)
	{
		super(id);
	}

	@Override
	public void init()
	{
		guiScene = new Stage();

		Table root = new Table();
		root.setFillParent(true);

		usernameField = new TextField("", RPGGame.getGameSkin());
		ipField = new TextField("", RPGGame.getGameSkin());
		portField = new TextField("", RPGGame.getGameSkin());
		connectButton = new TextButton("Connect", RPGGame.getGameSkin());
		backButton = new TextButton("Back", RPGGame.getGameSkin());

		usernameField.setMessageText("Username");
		ipField.setMessageText("IP");
		portField.setMessageText("Port");

		Table fieldPanel = new Table();
		Table buttonPanel = new Table();
		fieldPanel.defaults().width(300).height(30).padBottom(10).left();
		fieldPanel.add(usernameField);
		fieldPanel.row();
		fieldPanel.add(ipField);
		fieldPanel.row();
		fieldPanel.add(portField);
		buttonPanel.defaults().width(145).height(30).left();
		buttonPanel.add(connectButton).width(145).padRight(10);
		buttonPanel.add(backButton).width(145);

		buttonPanel.pack();

		root.add(fieldPanel);
		root.row();
		root.add(buttonPanel);

		guiScene.addActor(root);

		connectButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				try
				{
					connect(usernameField.getText(), ipField.getText(), Integer.parseInt(portField.getText()));
				} catch (NumberFormatException e)
				{
					Dialog d = new Dialog("Error", RPGGame.getGameSkin());
					d.text("Port must be a number!");
				}
			}
		});

		backButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				RPGGame.getInstance().getManager().popState();
			}
		});

	}

	public void connect(String user, String ip, int port)
	{
		MultiplayerSession session = new MultiplayerSession(user, ip, port);
		if (session.isConnected())
		{
			RPGGame.getInstance().getManager().pushState(new MultiplayerState(session));
		} else
		{
			System.err.println("Could not connect to " + session.getDestinationString());
		}
	}

	@Override
	public void resize(int width, int height)
	{
		guiScene.getViewport().update(width, height);
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void unpause()
	{

	}

	@Override
	public void update(float delta)
	{
		Gdx.input.setInputProcessor(guiScene);

		guiScene.act(delta);
	}

	@Override
	public void render()
	{
		guiScene.draw();
	}

	@Override
	public void dispose()
	{

	}

}

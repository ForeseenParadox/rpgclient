package draconic.rpggame.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import draconic.rpggame.RPGGame;

public class MainMenuState extends GameState
{

	public static final int BUTTON_WIDTH = 300;
	public static final int BUTTON_HEIGHT = 30;

	private Stage guiScene;
	private Table root;
	private Table menuButtons;
	private TextButton clientTestButton;
	private TextButton multiplayerButton;
	private TextButton joinDevServerButton;
	private TextButton worldEditorButton;
	private TextButton optionsButton;
	private TextButton quitButton;

	public MainMenuState(int id)
	{
		super(id);
	}

	@Override
	public void init()
	{
		guiScene = new Stage(new ScreenViewport(), RPGGame.getSpriteBatch());

		root = new Table(RPGGame.getGameSkin());
		root.setFillParent(true);
		root.setDebug(true);

		menuButtons = new Table(RPGGame.getGameSkin());
		menuButtons.setDebug(true);

		clientTestButton = new TextButton("Client Test", RPGGame.getGameSkin());
		multiplayerButton = new TextButton("Multiplayer", RPGGame.getGameSkin());
		worldEditorButton = new TextButton("World Editor", RPGGame.getGameSkin());
		joinDevServerButton = new TextButton("Join Dev Server", RPGGame.getGameSkin());
		optionsButton = new TextButton("Options", RPGGame.getGameSkin());
		quitButton = new TextButton("Quit", RPGGame.getGameSkin());

		menuButtons.defaults().width(BUTTON_WIDTH).height(BUTTON_HEIGHT).padBottom(10);
		menuButtons.padLeft(10);
		menuButtons.add(clientTestButton);
		menuButtons.row();
		menuButtons.add(multiplayerButton);
		menuButtons.row();
		menuButtons.add(worldEditorButton);
		menuButtons.row();
		menuButtons.add(joinDevServerButton);
		menuButtons.row();
		menuButtons.add(optionsButton);
		menuButtons.row();
		menuButtons.add(quitButton);

		root.add(menuButtons);
		root.bottom().left();
		guiScene.addActor(root);

		// add event handlers
		clientTestButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				RPGGame.getInstance().getManager().pushState(new ClientTestState());
			}
		});

		multiplayerButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				RPGGame.getInstance().getManager().pushState(RPGGame.STATE_MULTIPLAYER_LOGIN);
			}
		});

		worldEditorButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				System.out.println("Not yet implemented");
			}
		});

		joinDevServerButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				int randInt = (int) (Math.random() * 1000);
				MultiplayerSession session = new MultiplayerSession("User_" + String.format("%03d", randInt), "71.14.138.196", 25565);
				if (session.isConnected())
					RPGGame.getInstance().getManager().pushState(new MultiplayerState(session));
			}
		});

		optionsButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				RPGGame.getInstance().getManager().pushState(new OptionsState());
			}
		});

		quitButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Gdx.app.exit();
			}
		});
	}

	@Override
	public void resize(int width, int height)
	{
		guiScene.getViewport().update(width, height, true);
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void unpause()
	{

	}

	@Override
	public void update(float delta)
	{
		Gdx.input.setInputProcessor(guiScene);

		guiScene.act(delta);
	}

	@Override
	public void render()
	{
		guiScene.draw();
	}

	@Override
	public void dispose()
	{

	}

}

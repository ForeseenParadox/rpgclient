package draconic.rpggame.world;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import draconic.rpggame.RPGGame;
import draconic.rpggame.net.PacketType;
import draconic.rpggame.net.Peer;
import draconic.rpggame.util.RenderUtil;
import draconic.rpggame.world.entity.ClientEntityFactory;
import draconic.rpggame.world.entity.ClientPlayer;
import draconic.rpggame.world.entity.Entity;
import draconic.rpggame.world.entity.ItemEntity;
import draconic.rpggame.world.entity.MobileEntity;
import draconic.rpggame.world.entity.SizedEntity;

public class ClientWorld extends World
{

	private LayerRenderer layerRenderer;
	private TileRenderer tileRenderer;
	private Map<Long, Peer> peers;

	private Map<Integer, TextureRegion> tileTextures;
	private long clientId;
	private List<Entity> clientPredict;

	/**
	 * Creates a new client world with the specified name and tile size.
	 * 
	 * @param worldName
	 *            The name of the world
	 * @param tileSize
	 *            The width and height of tiles in this world
	 */
	public ClientWorld(String worldName, int tileSize)
	{
		super(worldName, tileSize);

		tileTextures = new HashMap<Integer, TextureRegion>();
		layerRenderer = new LayerRenderer();
		tileRenderer = new TileRenderer();
		peers = new HashMap<Long, Peer>();

		clientPredict = new ArrayList<Entity>();

		setUpdateEntities(false);
	}

	/**
	 * 
	 * @return The id of the client player
	 */
	public long getClientId()
	{
		return clientId;
	}

	/**
	 * @return The client player
	 */
	public ClientPlayer getClientPlayer()
	{
		return (ClientPlayer) getEntity(clientId);
	}

	/**
	 * 
	 * @param clientId
	 *            The id of the client player
	 */
	public void setClientId(long clientId)
	{
		this.clientId = clientId;
	}

	/**
	 * Adds a new peer.
	 * 
	 * @param peer
	 *            The peer to add
	 */
	public void addPeer(Peer peer)
	{
		peers.put(peer.getEntityId(), peer);
	}

	/**
	 * Adds a tile-texture mapping used when rendering layers.
	 * 
	 * @param id
	 *            The id of the tile
	 * @param texture
	 *            The texture that corresponds to the tile
	 */
	public void addTileTexture(int id, TextureRegion texture)
	{
		tileTextures.put(id, texture);
	}

	/**
	 * 
	 * @param entityId
	 *            The id of the peer
	 * @return The corresponding peer
	 */
	public Peer getPeer(long entityId)
	{
		return peers.get(entityId);
	}

	/**
	 * Removes the specified peer.
	 * 
	 * @param peer
	 *            The peer to remove
	 */
	public void removePeer(Peer peer)
	{
		peers.remove(peer.getEntityId());
	}

	/**
	 * Updates all entities locally that qualify for client prediction.
	 * 
	 * @param delta
	 */
	public void predict(float delta)
	{
		for (int i = 0; i < clientPredict.size(); i++)
			clientPredict.get(i).update(delta);
	}

	/**
	 * Updates this world based on information received from the server.
	 * 
	 * @param packetId
	 *            The id of the packet that was received
	 * @param dataIn
	 *            The data input stream used to read data
	 */
	public void networkUpdate(int packetId, DataInputStream dataIn)
	{
		try
		{
			if (packetId == PacketType.ENTITY_UPDATE)
			{
				long updateId = dataIn.readLong();

				// skip entity type
				dataIn.readInt();

				if (updateId == clientId)
				{
					dataIn.readFloat();
					dataIn.readFloat();
					dataIn.readInt();
					dataIn.readInt();
				} else
				{
					Entity e = getEntity(updateId);
					if (e != null)
						e.deserialize(dataIn);
				}

			} else if (packetId == PacketType.ENTITY_REMOVE)
			{
				long id = dataIn.readLong();
				Entity e = getEntity(id);
				removeEntity(e);
				if (clientPredict.contains(e))
					clientPredict.remove(e);
			} else if (packetId == PacketType.PLAYER_JOIN)
			{
				addPeer(new Peer(dataIn.readUTF(), dataIn.readLong()));
			} else if (packetId == PacketType.ENTITY_ADD)
			{
				long updateId = dataIn.readLong();
				int type = dataIn.readInt();

				Entity e = getEntity(updateId);
				if (e == null)
				{
					e = ClientEntityFactory.createEntity(updateId, type, this, dataIn);
					addEntity(e);
					if (type == Entity.PROJECTILE || type == Entity.PARTICLE)
						clientPredict.add(e);
				}

				e.deserialize(dataIn);

			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void update(float delta)
	{
		super.update(delta);
		predict(delta);
	}

	/**
	 * Renders debug items to the screen. This method assumes that the
	 * application is in debug mode and that the debug renderer is not in
	 * drawing mode at the time of method invocation.
	 * 
	 * @param debugRenderer
	 * @param worldCam
	 */
	private void renderDebug(ShapeRenderer debugRenderer, OrthographicCamera worldCam)
	{

		debugRenderer.setProjectionMatrix(worldCam.combined);
		debugRenderer.begin(ShapeType.Filled);
		Iterator<Entity> itr = getEntityIterator();
		while (itr.hasNext())
		{
			Entity e = itr.next();
			if (e instanceof SizedEntity)
			{
				RenderUtil.renderBox(debugRenderer, (SizedEntity) e);
				if (e instanceof MobileEntity)
					RenderUtil.renderHealthBar(debugRenderer, (MobileEntity) e, 150, 8);
			}
		}

		if (getClientPlayer().getCloseNpc() != null)
			RenderUtil.renderCircle(debugRenderer, getClientPlayer().getCloseNpc(), ClientPlayer.NPC_INTERACTION_RANGE);

		Iterator<ItemEntity> itemItr = getClientPlayer().getCloseItemIterator();
		while (itemItr.hasNext())
			RenderUtil.renderLine(debugRenderer, getClientPlayer(), itemItr.next());

		debugRenderer.end();

	}

	/**
	 * Renders this world. This method will also render debug information to the
	 * screen if the application is in debug mode. The world camera is assumed
	 * to already have been transformed as needed.
	 * 
	 * @param batch
	 *            The sprite batch used to render textures
	 * @param debugRenderer
	 *            The debug renderer used to render shapes
	 * @param worldCam
	 *            The world camera
	 */
	public void render(SpriteBatch batch, ShapeRenderer debugRenderer, OrthographicCamera worldCam)
	{

		// sort entities by z index so they are rendered in the proper order
		sortEntities();

		batch.setProjectionMatrix(worldCam.combined);
		batch.begin();

		// render tiles
		Iterator<Layer> layerIterator = getLayerIterator();
		while (layerIterator.hasNext())
			layerRenderer.render(layerIterator.next(), tileRenderer, getTileSize(), tileTextures, worldCam);

		// render entities
		Iterator<Entity> entityIterator = getEntityIterator();
		while (entityIterator.hasNext())
			entityIterator.next().render();

		batch.end();

		if (RPGGame.isDebug())
			renderDebug(debugRenderer, worldCam);
	}

}

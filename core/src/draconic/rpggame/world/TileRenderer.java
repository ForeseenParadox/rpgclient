package draconic.rpggame.world;

import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

import draconic.rpggame.RPGGame;

public class TileRenderer
{

	private SpriteBatch spriteBatch;
	private Vector3 translation;
	private Vector3 testVector;

	public TileRenderer()
	{
		spriteBatch = RPGGame.getSpriteBatch();

		// vector pools
		translation = new Vector3();
		testVector = new Vector3();
	}

	public void render(Tile tile, float x, float y, int tileSize, Map<Integer, TextureRegion> textureMap)
	{
		spriteBatch.getTransformMatrix().getTranslation(translation);
		testVector.setZero().add(x, y, 0).add(translation);

		float sx = spriteBatch.getProjectionMatrix().getScaleX(), sy = spriteBatch.getProjectionMatrix().getScaleY();
		float w = Gdx.graphics.getWidth() / sx, h = Gdx.graphics.getHeight() / sy;
		if (testVector.x >= -w / 2 - tileSize / sx && testVector.x < w / 2 && testVector.y >= -h / 2 - tileSize / sy && testVector.y < h / 2)
		{
			if (!spriteBatch.isDrawing())
				spriteBatch.begin();
			spriteBatch.draw(textureMap.get(tile.getTileId()), x, y, 0, 0, tileSize, tileSize, 1, 1, 0);
		}
	}

}

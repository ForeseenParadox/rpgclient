package draconic.rpggame.world;

import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

public class LayerRenderer
{

	public void render(Layer layer, TileRenderer renderer, int tileSize, Map<Integer, TextureRegion> textureMap, OrthographicCamera cam)
	{
		Tile[] tiles = layer.getTiles();
		int width = layer.getWidth(), height = layer.getHeight();

		// Vector3 unprojectedBl = cam.unproject(new Vector3(-Gdx.graphics.getWidth()/2, -Gdx.graphics.getHeight()/2, 0));
		Vector3 unprojectedBl = cam.unproject(new Vector3(-tileSize / cam.zoom, Gdx.graphics.getHeight() + tileSize / cam.zoom, 0));
		Vector3 unprojectedTr = cam.unproject(new Vector3(Gdx.graphics.getWidth() + tileSize / cam.zoom, -tileSize / cam.zoom, 0));
		int startX = (int) (unprojectedBl.x / tileSize), startY = (int) (unprojectedBl.y / tileSize), endX = (int) (unprojectedTr.x / tileSize), endY = (int) (unprojectedTr.y / tileSize);

		for (int x = startX; x < endX; x++)
			for (int y = startY; y < endY; y++)
				if (x >= 0 && y >= 0 && x < width && y < height && tiles[x + y * width] != null)
					renderer.render(tiles[x + y * width], (int) (x * tileSize), (y * tileSize), tileSize, textureMap);

	}

}

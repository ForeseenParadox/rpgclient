package draconic.rpggame.world;

import java.util.HashMap;
import java.util.Iterator;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

import draconic.rpggame.TextureUtils;

public class ClientWorldLoader extends WorldLoader
{

	@Override
	public ClientWorld loadWorld(String worldName, String path)
	{
		TiledMap map = new TmxMapLoader().load(path);
		TiledMapTileSets tileSets = map.getTileSets();
		Iterator<TiledMapTileSet> tileSetItr = tileSets.iterator();
		HashMap<Texture, Texture> tRegions = new HashMap<Texture, Texture>();
		int tileSize = (Integer) map.getProperties().get("tilewidth");
		ClientWorld result = new ClientWorld(worldName, tileSize);
		while (tileSetItr.hasNext())
		{
			TiledMapTileSet tileSet = tileSetItr.next();
			Iterator<TiledMapTile> tileItr = tileSet.iterator();
			while (tileItr.hasNext())
			{
				TiledMapTile t = tileItr.next();

				// add texture region mappings
				result.addTileTexture(t.getId(), t.getTextureRegion());

				if (!tRegions.containsKey(t.getTextureRegion().getTexture()))
				{
					tRegions.put(t.getTextureRegion().getTexture(), TextureUtils.replaceMagentaWithTransparency(t.getTextureRegion().getTexture()));
				}
				TextureRegion region = t.getTextureRegion();
				Texture tFixed = tRegions.get(t.getTextureRegion().getTexture());
				region.setTexture(tFixed);
			}
		}

		// add tile layers to world
		int layerCount = map.getLayers().getCount();
		for (int i = 0; i < layerCount; i++)
		{
			MapLayer tiledLayer = map.getLayers().get(i);
			if (tiledLayer instanceof TiledMapTileLayer)
			{
				TiledMapTileLayer layer = ((TiledMapTileLayer) map.getLayers().get(i));

				Layer l = new Layer(layer.getWidth(), layer.getHeight());
				for (int x = 0; x < layer.getWidth(); x++)
					for (int y = 0; y < layer.getHeight(); y++)
					{
						if (layer.getCell(x, y) != null)
						{
							TiledMapTile t = layer.getCell(x, y).getTile();
							l.getTiles()[x + y * l.getWidth()] = new Tile(t.getId());
						}
					}
				result.addLayer(layer.getName(), l);
			}
		}

		return result;
	}

}

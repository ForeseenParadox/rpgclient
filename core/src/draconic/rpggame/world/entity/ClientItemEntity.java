package draconic.rpggame.world.entity;

import java.io.DataInputStream;

import draconic.rpggame.gui.ItemImages;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.component.TextureComponent;

public class ClientItemEntity extends ItemEntity
{
	private TextureComponent textureComponent;

	public ClientItemEntity(World world, Vector2f position)
	{
		super(world, position);

		textureComponent = new TextureComponent(this);
		addEntityComponent(textureComponent);
	}

	public TextureComponent getTextureComponent()
	{
		return textureComponent;
	}

	@Override
	public void deserialize(DataInputStream input)
	{
		super.deserialize(input);
		
		textureComponent.setTexture(ItemImages.getItemTexture(getItemStack().getItem().getId()));
	}

}

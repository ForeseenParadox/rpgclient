package draconic.rpggame.world.entity.component;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import draconic.rpggame.RPGGame;
import draconic.rpggame.TextureAnimation;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.entity.EntityComponent;
import draconic.rpggame.world.entity.SizedEntity;

public class AnimationComponent extends EntityComponent
{

	private TextureAnimation animation;

	public AnimationComponent(SizedEntity entity, TextureAnimation animation)
	{
		super(entity);

		this.animation = animation;
	}

	public TextureAnimation getAnimation()
	{
		return animation;
	}

	public void setAnimation(TextureAnimation animation)
	{
		this.animation = animation;
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
		animation.update(delta);
	}

	@Override
	public void render()
	{
		super.render();
		SpriteBatch spriteBatch = RPGGame.getSpriteBatch();
		SizedEntity entity = (SizedEntity) getEntity();
		Vector2f pos = entity.getPosition();
		float w = entity.getScaledWidth(), h = entity.getScaledHeight();

		spriteBatch.draw(animation.getCurrentFrame(), pos.getX(), pos.getY(), entity.getWidth() / 2, entity.getHeight() / 2, w, h, 1, 1, entity.getRotation());
	}

}

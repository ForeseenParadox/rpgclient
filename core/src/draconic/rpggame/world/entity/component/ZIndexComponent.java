package draconic.rpggame.world.entity.component;

import draconic.rpggame.world.entity.EntityComponent;
import draconic.rpggame.world.entity.SizedEntity;

public class ZIndexComponent extends EntityComponent
{

	public ZIndexComponent(SizedEntity entity)
	{
		super(entity);
	}

	@Override
	public void render()
	{
		super.render();
		SizedEntity ent = (SizedEntity) getEntity();
		ent.setZ((int) (ent.getWorld().getPixelHeight() - ent.getMidY()));
	}

}

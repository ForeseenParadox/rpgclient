package draconic.rpggame.world.entity.component;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import draconic.rpggame.RPGGame;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.entity.Entity;
import draconic.rpggame.world.entity.EntityComponent;
import draconic.rpggame.world.entity.SizedEntity;

public class TextureComponent extends EntityComponent
{

	private TextureRegion texture;

	public TextureComponent(Entity entity)
	{
		super(entity);
	}

	public TextureComponent(SizedEntity entity, TextureRegion texture)
	{
		super(entity);
		this.texture = texture;
	}

	public TextureRegion getTexture()
	{
		return texture;
	}

	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	@Override
	public void render()
	{
		super.render();
		if (texture != null)
		{
			SpriteBatch spriteBatch = RPGGame.getSpriteBatch();
			SizedEntity entity = (SizedEntity) getEntity();
			Vector2f pos = entity.getPosition();
			float w = entity.getScaledWidth(), h = entity.getScaledHeight();

			spriteBatch.draw(texture, pos.getX(), pos.getY(), entity.getWidth() / 2, entity.getHeight() / 2, w, h, 1, 1, entity.getRotation());
		}
	}

}

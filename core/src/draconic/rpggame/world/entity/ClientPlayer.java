package draconic.rpggame.world.entity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector3;

import draconic.rpggame.RPGGame;
import draconic.rpggame.Resources;
import draconic.rpggame.TextureAnimation;
import draconic.rpggame.net.PacketType;
import draconic.rpggame.net.Peer;
import draconic.rpggame.state.MultiplayerState;
import draconic.rpggame.util.RenderUtil;
import draconic.rpggame.util.TimePoll;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.ClientWorld;
import draconic.rpggame.world.entity.component.AnimationComponent;

public class ClientPlayer extends Player
{

	public static final float NPC_INTERACTION_RANGE = 100;
	public static final float ITEM_INTERACTION_RANGE = 300;

	public static final float SPEED = 3f;

	private AnimationComponent animComponent;
	private TimePoll shootPoll;
	private TextureAnimation left, right, up, down;
	private boolean moved;

	private NPC closeNpc;
	private List<ItemEntity> closeItems;

	public ClientPlayer(ClientWorld world, Vector2f position)
	{
		super(world, position);

		left = RPGGame.getResourceHandler().getAnimation(Resources.PLAYER_LEFT).clone();
		right = RPGGame.getResourceHandler().getAnimation(Resources.PLAYER_RIGHT).clone();
		down = RPGGame.getResourceHandler().getAnimation(Resources.PLAYER_DOWN).clone();
		up = RPGGame.getResourceHandler().getAnimation(Resources.PLAYER_UP).clone();

		animComponent = new AnimationComponent(this, left);
		addEntityComponent(animComponent);

		shootPoll = new TimePoll(50);

		closeItems = new ArrayList<ItemEntity>();
	}

	public boolean moved()
	{
		return moved;
	}

	@Override
	public void setDirection(int direction)
	{
		super.setDirection(direction);
		AnimationComponent animComponent = getAnimationComponent();
		if (direction == Direction.WEST)
			animComponent.setAnimation(left);
		else if (direction == Direction.EAST)
			animComponent.setAnimation(right);
		else if (direction == Direction.NORTH)
			animComponent.setAnimation(up);
		else if (direction == Direction.SOUTH)
			animComponent.setAnimation(down);
	}

	public AnimationComponent getAnimationComponent()
	{
		return animComponent;
	}

	@Override
	public void serialize(DataOutputStream output, int header)
	{
		if (moved || header != PacketType.ENTITY_UPDATE)
		{
			setCurrentFrameIndex(animComponent.getAnimation().getCurrentFrameIndex());
			super.serialize(output, header);
		}
	}

	@Override
	public void deserialize(DataInputStream input)
	{
		super.deserialize(input);

		animComponent.getAnimation().setCurrentFrame(getCurrentFrameIndex());
	}

	public NPC getCloseNpc()
	{
		return closeNpc;
	}

	public Iterator<ItemEntity> getCloseItemIterator()
	{
		return closeItems.iterator();
	}

	private void handleMovement()
	{
		moved = false;

		if (Gdx.input.isKeyPressed(Keys.A))
		{
			setDirection(Direction.WEST);
			if (canMove(-SPEED, 0))
			{
				setX(getX() - SPEED);
				moved = true;
			}
		}
		if (Gdx.input.isKeyPressed(Keys.D))
		{
			setDirection(Direction.EAST);
			if (canMove(SPEED, 0))
			{
				setX(getX() + SPEED);
				moved = true;
			}
		}
		if (Gdx.input.isKeyPressed(Keys.W))
		{
			setDirection(Direction.NORTH);
			if (canMove(0, SPEED))
			{
				setY(getY() + SPEED);
				moved = true;
			}
		}
		if (Gdx.input.isKeyPressed(Keys.S) && canMove(0, -SPEED))
		{
			setDirection(Direction.SOUTH);
			if (canMove(0, -SPEED))
			{
				setY(getY() - SPEED);
				moved = true;
			}
		}
	}

	private void findCloseEntities()
	{
		Iterator<Entity> eList = getWorld().getEntityIterator();
		while (eList.hasNext())
		{
			Entity e = eList.next();
			if (e instanceof BasicNPC)
			{
				ClientBasicNPC npc = (ClientBasicNPC) e;
				if (npc.distance(this) < NPC_INTERACTION_RANGE)
					closeNpc = (BasicNPC) npc;
			} else if (e instanceof ItemEntity)
			{
				ItemEntity itemEnt = (ItemEntity) e;
				if (itemEnt.distance(this) < ITEM_INTERACTION_RANGE)
					closeItems.add(itemEnt);
			}
		}
	}

	private void handleShooting()
	{
		if (shootPoll.poll() && Gdx.input.isButtonPressed(Buttons.LEFT))
		{
			float mx = Gdx.input.getX(), my = Gdx.input.getY();
			float dx = mx - Gdx.graphics.getWidth() / 2, dy = my - Gdx.graphics.getHeight() / 2;
			float angle = (float) -(Math.atan2(dy, dx));
			float sin = (float) Math.sin(angle), cos = (float) Math.cos(angle);
			final float SPEED = 8;

			// send projectile information to server
			DataOutputStream out = MultiplayerState.getInstance().getSocketOutput();
			Projectile projectile = new Projectile(getWorld(), new Vector2f(getX() + (getScaledWidth() - (32 * 0.6f)) / 2, getY() + (getScaledHeight() - (32 * 0.6f)) / 2), 1, new Vector2f(cos * SPEED, sin * SPEED));
			projectile.setFiring(this);
			projectile.serialize(out, PacketType.ENTITY_ADD);
		}
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		handleMovement();
		findCloseEntities();
		handleShooting();

		int closeItemIndex = 0;
		while (closeItemIndex < closeItems.size())
		{
			ItemEntity itemEnt = closeItems.get(closeItemIndex);
			if (itemEnt.distance(this) > ITEM_INTERACTION_RANGE || !itemEnt.getWorld().containsEntity(itemEnt))
				closeItems.remove(closeItemIndex);
			else
				closeItemIndex++;
		}

		// check if the close npc is still in range
		if (closeNpc != null)
		{
			if (closeNpc.distance(this) > NPC_INTERACTION_RANGE)
				closeNpc = null;
			if (Gdx.input.isKeyJustPressed(Keys.Q))
			{

				// start dialog mode with close npc
				MultiplayerState current = (MultiplayerState) RPGGame.getInstance().getManager().getCurrent();

				BasicNPC e = (BasicNPC) closeNpc;
				current.startDialogMode(e.getDialog());
			}
		}

		if (!moved)
			animComponent.getAnimation().setCurrentFrame(0);
	}

	@Override
	public void render()
	{
		super.render();

		ClientWorld world = (ClientWorld) getWorld();
		long id = getSerialId();
		Peer p = world.getPeer(id);
		if (p != null && p.getEntityId() != world.getClientId())
		{
			String name = world.getPeer(id).getUsername();
			float width = RenderUtil.getStringWidth(RPGGame.getGameFont20(), name), height = RenderUtil.getStringHeight(RPGGame.getGameFont20(), name);
			RPGGame.getGameFont20().draw(RPGGame.getSpriteBatch(), name, (getMidX() - width / 2), getY() + getScaledHeight() + 5 + height);
		}

		Iterator<Entity> eList = getWorld().getEntityIterator();
		float mx = Gdx.input.getX(), my = Gdx.input.getY();
		Vector3 projected = new Vector3(mx, my, 0);
		MultiplayerState current = (MultiplayerState) RPGGame.getInstance().getManager().getCurrent();
		Vector3 unproject = current.getWorldCam().unproject(projected);
		while (eList.hasNext())
		{
			Entity e = eList.next();
			if (e instanceof MobileEntity && e != this)
			{
				MobileEntity ent = (MobileEntity) e;
				if (ent.contains(unproject.x, unproject.y))
				{
					String text = ent.getName() + " lvl " + ent.getLevel() + ", health: " + ent.getHealth() + "/" + ent.getMaxHealth();
					float width = RenderUtil.getStringWidth(RPGGame.getGameFont20(), text), height = RenderUtil.getStringHeight(RPGGame.getGameFont20(), text);
					RPGGame.getGameFont20().draw(RPGGame.getSpriteBatch(), text, (ent.getMidX() - width / 2), ent.getY() + ent.getScaledHeight() + 5 + height);
				}
			}
		}
	}

}

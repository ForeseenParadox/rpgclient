package draconic.rpggame.world.entity;

import draconic.rpggame.RPGGame;
import draconic.rpggame.Resources;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.component.TextureComponent;

public class ClientBasicNPC extends BasicNPC
{

	public ClientBasicNPC(World world)
	{
		super(world, new Vector2f());

		addEntityComponent(new TextureComponent(this, RPGGame.getResourceHandler().getAnimation(Resources.PLAYER_DOWN).getCurrentFrame()));
	}

}

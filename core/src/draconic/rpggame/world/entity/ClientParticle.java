package draconic.rpggame.world.entity;

import com.badlogic.gdx.graphics.Color;

import draconic.rpggame.RPGGame;
import draconic.rpggame.Resources;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.component.TextureComponent;

public class ClientParticle extends Particle
{

	public ClientParticle(World world, Vector2f position)
	{
		super(world, position);

		addEntityComponent(new TextureComponent(this, RPGGame.getResourceHandler().getTextureRegion(Resources.PARTICLE_TEXTURE)));
	}

	@Override
	public void render()
	{
		Color prev = RPGGame.getSpriteBatch().getColor();
		float alpha = Math.max(0, (1 - (float) getTicksLived() / getLife()));
		RPGGame.getSpriteBatch().setColor(1, 1, 1, alpha);
		super.render();
		RPGGame.getSpriteBatch().setColor(prev);
	}

}

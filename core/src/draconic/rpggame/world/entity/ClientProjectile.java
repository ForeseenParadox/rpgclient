package draconic.rpggame.world.entity;

import draconic.rpggame.RPGGame;
import draconic.rpggame.Resources;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.component.TextureComponent;

public class ClientProjectile extends Projectile
{

	public ClientProjectile(World world, Vector2f position, int damage, float dir, float mag)
	{
		super(world, position, damage, dir, mag);
		addEntityComponent(new TextureComponent(this, RPGGame.getResourceHandler().getTextureRegion(Resources.PROJECTILE_TEXTURE)));
	}

	public ClientProjectile(World world, Vector2f position, int damage, Vector2f velocity)
	{
		super(world, position, damage, velocity);
		addEntityComponent(new TextureComponent(this, RPGGame.getResourceHandler().getTextureRegion(Resources.PROJECTILE_TEXTURE)));
	}

}

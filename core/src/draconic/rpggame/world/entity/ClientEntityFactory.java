package draconic.rpggame.world.entity;

import java.io.DataInputStream;

import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.ClientWorld;
import draconic.rpggame.world.entity.component.ZIndexComponent;

public class ClientEntityFactory
{

	public static Entity createEntity(long id, int type, ClientWorld parent, DataInputStream input)
	{
		Entity result = null;
		if (type == Entity.SLIME)
		{
			result = new ClientSlime(parent, new Vector2f());
		} else if (type == Entity.PLAYER)
		{
			result = new ClientPlayer(parent, new Vector2f());
		} else if (type == Entity.PROJECTILE)
		{
			result = new ClientProjectile(parent, new Vector2f(), 1, new Vector2f());
		} else if (type == Entity.ITEM)
		{
			result = new ClientItemEntity(parent, new Vector2f());
		} else if (type == Entity.MOB_SPAWNER)
		{
			result = new MobSpawner(parent, new Vector2f(), 0, 0);
		} else if (type == Entity.PARTICLE)
		{
			result = new ClientParticle(parent, new Vector2f());
		} else if (type == Entity.NPC)
		{
			result = new ClientBasicNPC(parent);
		}

		if (result instanceof SizedEntity)
			result.addEntityComponent(new ZIndexComponent((SizedEntity) result));

		if (result != null)
		{
			result.assignSerialId(id);
		} else
		{
			// type not found
			System.err.println("Entity type " + type + " not found! Unable to properly construct entity.");
		}

		return result;

	}

}

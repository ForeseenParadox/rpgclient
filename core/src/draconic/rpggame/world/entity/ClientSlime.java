package draconic.rpggame.world.entity;

import java.io.DataInputStream;

import com.badlogic.gdx.graphics.Color;

import draconic.rpggame.RPGGame;
import draconic.rpggame.Resources;
import draconic.rpggame.util.ColorUtil;
import draconic.rpggame.util.Vector2f;
import draconic.rpggame.world.World;
import draconic.rpggame.world.entity.component.AnimationComponent;

public class ClientSlime extends Slime
{

	private AnimationComponent animation;
	private Color color;

	public ClientSlime(World world, Vector2f position)
	{
		super(world, position);

		animation = new AnimationComponent(this, RPGGame.getResourceHandler().getAnimation(Resources.SLIME_ANIMATION));

		addEntityComponent(animation);

		color = ColorUtil.createHSVColor((int) (Math.random() * 360), 1, 1, 0.7f);
	}

	@Override
	public void deserialize(DataInputStream input)
	{
		super.deserialize(input);

		animation.getAnimation().setCurrentFrame(getAnimator().getCurrentFrameIndex());
	}

	@Override
	public void render()
	{
		Color current = RPGGame.getSpriteBatch().getColor();
		RPGGame.getSpriteBatch().setColor(new Color(current).mul(color));
		super.render();
		RPGGame.getSpriteBatch().setColor(current);
	}

}

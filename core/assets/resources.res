<Content>

	<!-- Spritesheet definitions -->
	
	<SpriteSheet id="Projectiles" src="Textures/Projectiles.png" spriteWidth="16" spriteHeight="16" transparency="FF00FF">
		<Texture id="Projectile_1" x="0" y="0" width="1" height="1" subX="1" subY="1" subWidth="14" subHeight="14"/>	
		<Texture id="Particle_1" x="1" y="0" width="1" height="1" subX="5" subY="5" subWidth="6" subHeight="6"/>
	</SpriteSheet>
	
	<SpriteSheet id="Player" src="Textures/Player_Test.png" spriteWidth="32" spriteHeight="32" transparency="FF00FF"/>
	<SpriteSheet id="Enemies" src="Textures/Enemies.png" spriteWidth="32" spriteHeight="32" transparency="FF00FF"/>
	
	<!-- Animation definitions -->
	
	<Animation id="Slime" delay="200" src="Enemies" x="0" y="0" spritesWide="3" spritesTall="1" cellWidth="32" cellHeight="32" subX="5" subY="8" subWidth="24" subHeight="17">
		0,1,2,1
	</Animation>
	
	<Animation id="Player_Up" src="Player" delay="200" x="0" y="0" spritesWide="3" spritesTall="1" cellWidth="32" cellHeight="32" subX="6" subY="4" subWidth="18" subHeight="27"/>
	<Animation id="Player_Down" src="Player" delay="200" x="0" y="1" spritesWide="3" spritesTall="1" cellWidth="32" cellHeight="32" subX="6" subY="4" subWidth="18" subHeight="27"/>
	<Animation id="Player_Right" src="Player" delay="200" x="0" y="2" spritesWide="3" spritesTall="1" cellWidth="32" cellHeight="32" subX="6" subY="4" subWidth="18" subHeight="27"/>	
	<Animation id="Player_Left" src="Player" delay="200" x="0" y="3" spritesWide="3" spritesTall="1" cellWidth="32" cellHeight="32" subX="6" subY="4" subWidth="18" subHeight="27"/>

</Content>